from flask import Flask, render_template, g, request, redirect, url_for
from Crypto.Hash import SHA256
import random
import ConfigParser
import logging
from logging.handlers import RotatingFileHandler

app = Flask(__name__)

import sqlite3

db_location = 'var/test.db'

def get_db():
    db = getattr(g, 'db', None)
    if db is None:
        db = sqlite3.connect(db_location)
        g.db = db
    return db

@app.teardown_appcontext
def close_db_connection(exception):
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()

#if url has no additional content, display index page
@app.route('/')
def index():
  app . logger . info ("Homepage reached")
  return render_template('index.html', title='Home')
  
@app.route('/starters', methods=['GET', 'POST'])
def starters():
  app.logger.info("Starters page reached")
  db = get_db()
  cur = db.cursor()
  ingList = list()
  foodList = list()
	
  if request.method == 'POST':
	dispChicken = request.form.get('Chicken')
	dispBeef = request.form.get('Beef')
	dispSea = request.form.get('Seafood')
	dispVeg = request.form.get('Vegetarian')
	dispStarter = request.form.get('Starter')
	dispSide = request.form.get('Side')
	
	cur.execute("DELETE FROM tempMenu")
	
	if dispChicken:
		cur.execute("SELECT * FROM starters WHERE ingredients = 'Chicken'")
		ingList = cur.fetchall()
		app.logger.info("display chicken ingredient selected")
	if dispBeef:
		cur.execute("SELECT * FROM starters WHERE ingredients = 'Beef'")
		ingList += cur.fetchall()
		app.logger.info("display beef ingredient selected")
	if dispSea:
		cur.execute("SELECT * FROM starters WHERE ingredients = 'Seafood'")
		ingList += cur.fetchall()
		app.logger.info("display seafood ingredient selected")
	if dispVeg:
		cur.execute("SELECT * FROM starters WHERE ingredients = 'Vegetarian'")
		ingList += cur.fetchall()
		app.logger.info("display vegetarian ingredient selected")
	if not dispChicken and not dispBeef and not dispSea and not dispVeg:
		cur.execute("SELECT * FROM starters")
		ingList = cur.fetchall()
		app.logger.info("no ingredient selected, display all")
		
	for row in ingList:
		cur.execute("INSERT INTO tempMenu VALUES (?, ?, ?, ?);", (row[0], row[1], row[2], row[3]))
	
	if dispStarter:
		cur.execute("SELECT * FROM tempMenu WHERE category = 'starter'")
		foodList = cur.fetchall()
		app.logger.info("display starters selected")
	if dispSide:
		cur.execute("SELECT * FROM tempMenu WHERE category = 'side'")
		foodList += cur.fetchall()
		app.logger.info("display sides selected")
	if not dispSide and not dispStarter:
		cur.execute("SELECT * FROM tempMenu")
		foodList = cur.fetchall()
		app.logger.info("no dish filter selected, display all")

  else:
	cur.execute("SELECT * FROM starters")
	foodList = cur.fetchall()
	app.logger.info("Form not submitted, display all")
  
  if len(foodList) == 0:
	noFood = [' ' , ' ','There is nothing to display']
	foodList.append(noFood)
	app.logger.info("Filters applied, nothing to display")
	
  return render_template('starters.html', title='Starters', starterTable = foodList)
  
  
@app.route('/mains', methods=['GET', 'POST'])
def mains():
  app.logger.info("Mains page reached")
  db = get_db()
  cur = db.cursor()
  ingList = list()
  foodList = list()
	
  if request.method == 'POST':
	dispChicken = request.form.get('Chicken')
	dispBeef = request.form.get('Beef')
	dispSea = request.form.get('Seafood')
	dispVeg = request.form.get('Vegetarian')
	dispRice = request.form.get('Rice')
	dispChow = request.form.get('Chow')
	dispSoup = request.form.get('Soup')
	dispSweet = request.form.get('Sweet')
	dispCurry = request.form.get('Curry')
	dispSalt = request.form.get('Salt')
	dispStir = request.form.get('Stir')
	dispEuro = request.form.get('Euro')
	
	cur.execute("DELETE FROM tempMenu")
	
	if dispChicken:
		cur.execute("SELECT * FROM Mains WHERE ingredients = 'Chicken'")
		ingList = cur.fetchall()
		app.logger.info("display chicken ingredient selected")
	if dispBeef:
		cur.execute("SELECT * FROM Mains WHERE ingredients = 'Beef'")
		ingList += cur.fetchall()
		app.logger.info("display beef ingredient selected")
	if dispSea:
		cur.execute("SELECT * FROM Mains WHERE ingredients = 'Seafood'")
		ingList += cur.fetchall()
		app.logger.info("display seafood ingredient selected")
	if dispVeg:
		cur.execute("SELECT * FROM Mains WHERE ingredients = 'Vegetarian'")
		ingList += cur.fetchall()
		app.logger.info("display vegetarian ingredient selected")
	if not dispChicken and not dispBeef and not dispSea and not dispVeg:
		cur.execute("SELECT * FROM Mains")
		ingList = cur.fetchall()
		app.logger.info("no ingredient selected, display all")
		
	for row in ingList:
		cur.execute("INSERT INTO tempMenu VALUES (?, ?, ?, ?);", (row[0], row[1], row[2], row[3]))
	
	if dispRice:
		cur.execute("SELECT * FROM tempMenu WHERE category = 'Rice'")
		foodList = cur.fetchall()
		app.logger.info("display rice foods selected")
	if dispChow:
		cur.execute("SELECT * FROM tempMenu WHERE category = 'Chow mein'")
		foodList += cur.fetchall()
		app.logger.info("display chow mein foods selected")
	if dispSoup:
		cur.execute("SELECT * FROM tempMenu WHERE category = 'Soup'")
		foodList += cur.fetchall()
		app.logger.info("display soup foods selected")
	if dispSweet:
		cur.execute("SELECT * FROM tempMenu WHERE category = 'Sweet & sour'")
		foodList += cur.fetchall()
		app.logger.info("display sweet & sour foods selected")
	if dispCurry:
		cur.execute("SELECT * FROM tempMenu WHERE category = 'Curry'")
		foodList += cur.fetchall()
		app.logger.info("display curry foods selected")
	if dispSalt:
		cur.execute("SELECT * FROM tempMenu WHERE category = 'Salt & spicy'")
		foodList += cur.fetchall()
		app.logger.info("display salt & spicy foods selected")
	if dispStir:
		cur.execute("SELECT * FROM tempMenu WHERE category = 'Stir fry'")
		foodList += cur.fetchall()
		app.logger.info("display stir fry foods selected")
	if dispEuro:
		cur.execute("SELECT * FROM tempMenu WHERE category = 'European'")
		foodList += cur.fetchall()
		app.logger.info("display european foods selected")
	if not dispRice and not dispChow and not dispSoup and not dispSweet and not dispCurry and not dispSalt and not dispStir and not dispEuro:
		cur.execute("SELECT * FROM tempMenu ORDER BY category")
		foodList = cur.fetchall()
		app.logger.info("no food type chosen, display all")
  else:
	app.logger.info("Form not submitted, display all")
	cur.execute("SELECT * FROM Mains ORDER BY category")
	foodList = cur.fetchall()
	
  if len(foodList) == 0:
	app.logger.info("Filters applied, nothing to display")
	noFood = [' ' , ' ','There is nothing to display']
	foodList.append(noFood)
	
  return render_template('mains.html', title='Mains', mainTable = foodList)
 
@app.route('/desserts', methods=['GET', 'POST'])
def desserts():
  app.logger.info("desserts page reached")
  db = get_db()
  cur = db.cursor()
  dessertList = list()
	
  if request.method == 'POST':
	dispFruit = request.form.get('Fruit')
	dispCake = request.form.get('Cake')
	dispIce = request.form.get('Ice')
	
	cur.execute("DELETE FROM tempMenu")
	
	if dispFruit:
		cur.execute("SELECT * FROM desserts WHERE ingredient = 'Fruit'")
		dessertList = cur.fetchall()
		app.logger.info("display fruit selected")
	if dispCake:
		cur.execute("SELECT * FROM desserts WHERE ingredient = 'Cake'")
		dessertList += cur.fetchall()
		app.logger.info("display cake selected")
	if dispIce:
		cur.execute("SELECT * FROM desserts WHERE ingredient = 'Icecream'")
		dessertList += cur.fetchall()
		app.logger.info("display icecream selected")
	if not dispFruit and not dispCake and not dispIce:
		cur.execute("SELECT * FROM desserts")
		dessertList = cur.fetchall()
		app.logger.info("no filters selected, display all")
  else:
	cur.execute("SELECT * FROM desserts")
	dessertList = cur.fetchall()
		
  return render_template('desserts.html', title='Desserts', dessertTable = dessertList)

@app.route('/drinks', methods=['GET', 'POST'])
def drinks():
  app.logger.info("drinks page reached")
  db = get_db()
  cur = db.cursor()
  drinkList = list()
	
  if request.method == 'POST':
	dispSoft = request.form.get('Soft')
	dispSpirit = request.form.get('Spirit')
	dispBeer = request.form.get('Beer')
	dispCider = request.form.get('Cider')
	dispWine = request.form.get('Wine')
	dispHot = request.form.get('Hot')
	
	cur.execute("DELETE FROM tempMenu")
	
	if dispSoft:
		cur.execute("SELECT * FROM drinks WHERE drink = 'Soft Drink'")
		drinkList = cur.fetchall()
		app.logger.info("display soft drinks selected")
	if dispSpirit:
		cur.execute("SELECT * FROM drinks WHERE drink = 'Spirit'")
		drinkList += cur.fetchall()
		app.logger.info("display spirits selected")
	if dispBeer:
		cur.execute("SELECT * FROM drinks WHERE drink = 'Beer'")
		drinkList += cur.fetchall()
		app.logger.info("display beer selected")
	if dispCider:
		cur.execute("SELECT * FROM drinks WHERE drink = 'Cider'")
		drinkList += cur.fetchall()
		app.logger.info("display cider selected")
	if dispWine:
		cur.execute("SELECT * FROM drinks WHERE drink = 'Wine'")
		drinkList += cur.fetchall()
		app.logger.info("display wine selected")
	if dispHot:
		cur.execute("SELECT * FROM drinks WHERE drink = 'Hot Drink'")
		drinkList += cur.fetchall()
		app.logger.info("display hot drinks selected")
	if not dispSoft and not dispSpirit and not dispBeer and not dispCider and not dispWine and not dispHot:
		cur.execute("SELECT * FROM drinks")
		drinkList = cur.fetchall()
		app.logger.info("no filters selected, display all")
  else:
	cur.execute("SELECT * FROM drinks")
	drinkList = cur.fetchall()

  return render_template('drinks.html', title='Drinks', drinkTable = drinkList)

@app.route('/booking', methods=['GET', 'POST'])
def booking():
	app.logger.info("booking page reached")
	db = get_db()
	cur = db.cursor()
	
	if request.method == 'POST':
		date = request.form.get('Date')
		guests = request.form.get('guests')
		name = request.form.get('name')
		email = request.form.get('email')		
		time = request.form.get('Time')	
		custReference = request.form.get('reference')
		
		if custReference:
			custReference = str(custReference)
			hashedCustRef = SHA256.new(custReference).hexdigest()
			cur.execute("SELECT * FROM bookings WHERE reference = (?) ", (hashedCustRef,))
			count = cur.fetchone()
			if count is None:
				#return unrecognised ref
				app.logger.info("booking cancellation attempted, reference not recognised : "+custReference)
				return render_template('not-recognised.html', title='Booking')
			else:
				#return booking cancelled
				app.logger.info("booking cancelled reference : "+custReference)
				cur.execute("DELETE FROM bookings WHERE reference = (?) ", (hashedCustRef,))
				db.commit()
				return render_template('cancelled.html', title='Booking')
		
		if not email or not name or not guests or not date:
			return render_template('part-filled.html', title='Booking')
			app.logger.info("booking attempted, not all fields filled")
		else:
			inDatabase = 1
			while(inDatabase == 1):
				reference = random.randint(1000000, 9999999)
				reference = str(reference)
				hashedRef = SHA256.new(reference).hexdigest()
				cur.execute("SELECT * FROM bookings WHERE reference = (?) ", (hashedRef,))
				count = cur.fetchone()
				if count is None:
					inDatabase = 2
			cur.execute("SELECT * FROM bookings WHERE time = ? AND date = ?", (time, date))
			count = cur.fetchone()
			if count is None:
				#show ref and say booked
				app.logger.info("booking successful")
				cur.execute("INSERT INTO bookings VALUES (?, ?, ?, ?, ?, ?);", (hashedRef, name, email, date, time, guests))
				db.commit()
				return render_template('successful.html', title='Booking', bookingRef = reference)
			else:
				#ask to make new booking
				app.logger.info("booking attempted, no space on chosen date + time")
				return render_template('no-space.html', title='Booking')
	else:
		return render_template('booking.html', title='Booking')

		
def init(app):
    config = ConfigParser.ConfigParser()
    try:
        config_location = "etc/logging.cfg"
        config.read(config_location)

        app.config['DEBUG'] = config.get("config", "debug")
        app.config['ip_address'] = config.get("config", "ip_address")
        app.config['port'] = config.get("config", "port")
        app.config['url'] = config.get("config", "url")
        app.config['log_file'] = config.get("logging", "name")
        app.config['log_location'] = config.get("logging", "location")
        app.config['log_level'] = config.get("logging", "level")
    except:
        print "Could not read configs from: ", config_location

def logs(app):
    log_pathname = app.config['log_location'] + app.config['log_file']
    file_handler = RotatingFileHandler(log_pathname, maxBytes=1024*1024 * 10 ,  backupCount=1024)
    file_handler.setLevel( app.config['log_level'] )
    formatter = logging.Formatter("%(levelname)s | %(asctime)s | %(module)s | %(funcName)s | %(message)s")
    file_handler.setFormatter(formatter)
    app.logger.setLevel( app.config['log_level'] )
    app.logger.addHandler(file_handler)

if __name__ == '__main__':
    init(app)
    logs(app)
    app.run(
        host=app.config['ip_address'],
        port=int(app.config['port']))
